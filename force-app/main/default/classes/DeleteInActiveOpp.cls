/**
 * 1. Create a Custom Picklist field on Account with Active, In Active, Hold.
 * 2. If the Account status is changed to In Active or Hold. Delete all the Close Won Opportunities
      to the related Account
 */

public class DeleteInActiveOpp {
	
    @InvocableMethod(label = 'Delete Related Opportunities'
                     description = 'Delete Related Opportunities with In Active or Hold Account Status'
                     category = 'Apex')
    public static void deleteOpp(List<String> ids){
        List<Opportunity> oppList = [SELECT Id 
                                     FROM Opportunity 
                                     WHERE AccountId =: ids
                                     AND StageName = 'Closed Won'];
        
    	delete oppList;
    }
    
}