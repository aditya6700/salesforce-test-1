@isTest
public class ForexApi_Test {
    //@TestSetup
    //public static void testData(){
        
    //}
    
	@isTest
    public static void successTest(){
        Test.startTest();
        	
        	Test.setMock(HttpCalloutMock.class, new ForexHttpMockSuccess_Test());
        	ForexApi.getData();
        
        Test.stopTest();    
    }
    
    @isTest
    public static void errorTest(){
        Test.startTest();
        	
        	Test.setMock(HttpCalloutMock.class, new ForexHttpMockError_Test());
        	ForexApi.getData();
        
        Test.stopTest();
    }
    
    @isTest
    public static void HttpError(){
        Test.startTest();
			ForexApi.getData();
        Test.stopTest();
    }
}