@isTest
public class ForexHttpMockError_Test implements HttpCalloutMock {
    public HttpResponse respond(HttpRequest request){
        HttpResponse response = new HttpResponse();
        String JsonBody = '{'+
		'    "success": false,'+
		'    "error": {'+
		'        "code": 104,'+
		'        "info": "Your monthly usage limit has been reached. Please upgrade your subscription plan."    '+
		'  }'+
		'} ';
        response.setBody(JsonBody);
        response.setStatusCode(104);
        return response;
    }
}