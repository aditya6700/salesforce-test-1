public class ForexApi{

    @InvocableMethod(label = 'Get Data from ForexApi'
                     description = 'Currency and Exchange rates are fetched from ForexApi and updated'
                     category = 'Apex')
    public static void getData(){
        
        try {
            String baseUrl = 'http://data.fixer.io/api/latest?access_key=';
        	String accessKey = 'b1a85a3d881a2dc784f58c83466ad608';
            
            HTTP http = new Http();
            HttpRequest request = new HttpRequest();

            request.setEndpoint(baseUrl + accesskey);
            request.setMethod('GET');
            request.setHeader('Content-Type', 'application/json');

            HttpResponse response = http.send(request);

            String stat = response.getStatus();
            Integer statCode = response.getStatusCode();

            System.debug('STATUS:'+stat); System.debug('STATUS_CODE:'+statCode); 

            if (statCode == 200) 
                dataManupulate(response);
           	else
                errorhandler(stat);

        } catch (Exception e) {
            System.debug('There is some error while getting the response'+e);
        }    
    }


    public static void dataManupulate(HttpResponse response) {
        
        Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
        Map<String, Object> rates = (Map<String, Object>)(results.get('rates'));
        
        // List<Exchange_Rates__c> updateList = new List<Exchange_Rates__c>(); 
        // List<Exchange_Rates__c> insertList = new List<Exchange_Rates__c>(); 
        
        // for (Exchange_Rates__c ids: [SELECT Currency__c, Rate__c FROM Exchange_Rates__c]) {
        
        //     if (rates.containsKey(ids.Currency__c)) {
        //         ids.Rate__c = (Decimal)rates.get(ids.Currency__c);
        //         updateList.add(ids);
        //     }else{
        //         Exchange_Rates__c sample = new Exchange_Rates__c();
        
        //         sample.Currency__c = ids.Currency__c;
        //         sample.Rate__c = (Decimal)rates.get(ids.Currency__c);
        //         insertList.add(sample);
        //     }
        // }
        
        // if (!updateList.isEmpty()) {
        //     update updateList;
        //     System.debug('updated data');
        // } else {
        //     insert insertList;
        //     System.debug('inserted data');
        // }
        
        List<Exchange_Rates__c> insertList = new List<Exchange_Rates__c>(); 
        
        List<Exchange_Rates__c> orgList = [SELECT Currency__c, Rate__c FROM Exchange_Rates__c];
        delete orgList;
        
        for(String rate: rates.KeySet()){                
            Exchange_Rates__c sample = new Exchange_Rates__c();
            
            sample.Currency__c = rate;
            sample.Rate__c = (Decimal)rates.get(rate);
            insertList.add(sample);
        }
        
        insert insertList;
        
        System.debug('Executed successfully'); 
    }

  
    public static void errorhandler(String e){
        System.debug('There is some error while getting the response');
        System.debug('There status of the response is '+e);
    }
}